#!/usr/bin/python3
from flask import Flask, jsonify, redirect, render_template, request, session
from elasticsearch import Elasticsearch, exceptions as es_exceptions
import pyrebase
import requests
import logging
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from urlextract import URLExtract
import time
from datetime import datetime
from elasticsearch.helpers import bulk
from unidecode import unidecode
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
import asyncio
import os
from dotenv import load_dotenv


analyzer_name = "mi_analizador"
logging.basicConfig(level=logging.INFO)


# Establecer el nivel de log para el módulo elastic_transport.transport
logging.getLogger('elastic_transport.transport').setLevel(logging.ERROR)
# También puedes usar logging.WARNING en lugar de logging.ERROR si deseas ver advertencias también

# Desactivar otras advertencias de urllib3
urllib3_logger = logging.getLogger('urllib3')
urllib3_logger.setLevel(logging.ERROR)
    

# Cargar las variables de entorno desde el archivo .env
load_dotenv()

# Obtener los valores de las variables
ELASTIC_PASSWORD = os.getenv("ELASTIC_PASSWORD")
ELASTIC_CLOUD_ID = os.getenv("ELASTIC_CLOUD_ID")
ELASTIC_USERNAME = os.getenv("ELASTIC_USERNAME")
API_KEY= os.getenv("API_KEY")
AUTH_DOMAIN = os.getenv("AUTH_DOMAIN")
PROJECT_ID = os.getenv("PROJECT_ID")
STORAGE_BUCKET = os.getenv("STORAGE_BUCKET")
MESSAGING_SENDER_ID= os.getenv("MESSAGING_SENDER_ID")
APP_ID= os.getenv("APP_ID")
# Crear la instancia del cliente
es = Elasticsearch(
    cloud_id=ELASTIC_CLOUD_ID,
    basic_auth=(ELASTIC_USERNAME, ELASTIC_PASSWORD),
)


try:
    es.info()
except:
    print(f"Error de autenticación: ")

app = Flask(__name__)

# Firebase Config
config = {
    'apiKey': API_KEY,
    'authDomain': AUTH_DOMAIN,
    'projectId': PROJECT_ID,
    'storageBucket': STORAGE_BUCKET,
    'messagingSenderId': MESSAGING_SENDER_ID,
    'appId': APP_ID,
    'databaseURL': ""
}
firebase = pyrebase.initialize_app(config)
auth = firebase.auth()

app.secret_key = 'secret'
error = "Datos ingresados incorrectos, vuelva a intentar"

# Variable global para almacenar el progreso
progress = 0

# Variable global para la Flag de cancelación de indexación
cancel_indexing_flag = False

# Chrome options
chrome_options = Options()
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("--disable-software-rasterizer")
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--disable-blink-features=AutomationControlled")
chrome_options.add_argument("--disable-web-security")
chrome_options.add_argument("--disable-extensions")
chrome_options.add_argument("--disable-dev-shm-usage")
chrome_options.add_argument("--disable-features=NetworkService")
chrome_options.add_argument("--headless")

prefs = {"profile.managed_default_content_settings.images": 2}
chrome_options.add_experimental_option("prefs", prefs)


@app.route('/')
def home():
    if 'user' in session:
        print("UID USER: \t" + session['user'][1])
        return render_template("home.html")
    else:
        return render_template("welcome.html")

@app.route('/login', methods=['POST'])
def login():
    data = request.json
    email = data.get('email')
    password = data.get('password')
    try:
        user = auth.sign_in_with_email_and_password(email, password)
        details = auth.get_account_info(user['idToken'])
        for resul in details['users']:
            uid = resul['localId']
        session['user'] = email, uid.lower()
        return jsonify({"success": True})  # Indica un inicio de sesión exitoso
    except requests.exceptions.HTTPError as e:
        error_message = str(e)
        if 'INVALID_PASSWORD' in error_message:
            error_message = "Contraseña incorrecta. Por favor, verifique sus credenciales."
        else:
            error_message = "Ocurrió un error durante el inicio de sesión."
        print("Error al logearse:", error_message)
        return jsonify({"success": False, "error": error_message})
    except Exception as e:
        print("Ocurrió un error inesperado durante el inicio de sesión:", str(e))
        return jsonify({"success": False, "error": "Ocurrió un error inesperado durante el inicio de sesión"})

@app.route('/logout')
def logout():
    session.pop('user')
    return render_template("welcome.html")

@app.route('/register', methods=['GET', 'POST'])
def register():
    return render_template("register.html")

@app.route('/signup', methods=['GET'])
def signUp():
    email = request.args.get('email')
    password = request.args.get('password')
    try:
        auth.create_user_with_email_and_password(email, password)
    except:
        return render_template("error.html", error)
    return render_template("home.html")

@app.route('/home')
def searchView():
    if 'user' in session:
        print("UID USER: \t" + session['user'][1])
        return render_template("home.html")
    else:
        return render_template("welcome.html")

@app.route('/index')
def insertSiteMap():
    if 'user' in session:
        return render_template("index.html")
    else:
        return render_template("welcome.html")
    

@app.route('/indexing', methods=['GET', 'POST'])
def indexPages():
    
    if 'user' not in session:
        return redirect('/')

    sitemap = request.args.get('webSiteMap')
    index_name = session['user'][1]
    index_settings = None

    try:
        # Obtener los enlaces de la página
        links_from_page = get_links_from_page(sitemap)

        # Verificar si links_from_page no es None antes de intentar obtener la longitud
        if links_from_page is not None:
            total_pages = len(links_from_page)
            
            # Verificar si new_documents no es None antes de iterar sobre él
            new_documents = asyncio.run(scrape_and_index(links_from_page, index_name, analyzer_name, request))
            if new_documents is not None:
                for doc in new_documents:
                    try:
                        existing_doc = es.get(index=index_name, id=doc['_id'])
                        es.update(index=index_name, id=doc['_id'], body={"doc": doc['_source']})
                    except es_exceptions.NotFoundError:
                        es.index(index=index_name, body=doc['_source'])

                return redirect('/home')
            else:
                print("Error al obtener documentos. La función scrape_and_index devolvió None.")
                return "Error al obtener documentos. La función scrape_and_index devolvió None."
        else:
            # Manejar el caso cuando links_from_page es None
            print("Error al obtener enlaces de la página")
            return "Error al obtener enlaces de la página"

    except es_exceptions.TransportError as transport_error:
        if '_source' in str(transport_error):
            # Capturar la excepción específica y mostrar un mensaje
            print("Se capturó la excepción '_source'. Continuando con la ejecución.")
            return redirect('/home')
        else:
            # Manejar otras excepciones
            return f"Error al ejecutar el rastreo e indexación: {str(transport_error)}"
    except Exception as e:
        print(f"Error inesperado: {str(e)}")
        return redirect('/home')

@app.route('/process')
def process():
    return render_template("process.html")

@app.route('/delete')
def delete():
    es.indices.delete(index='*', ignore=[400, 404])
    return render_template("home.html")

@app.route('/results', methods=['GET', 'POST'])
def searchIn():

    if 'user' not in session:
        return redirect('/')
    
    query = request.args.get('searchContent')
    indexName = session['user'][1]

    transformed_query = transform_query(query, indexName, analyzer_name)
    print(transformed_query)

    try:
        res = es.search(index=indexName, body={
            "query": {
                "bool": {
                    "should": [
                        {
                            "function_score": {
                                "query": {
                                    "bool": {
                                        "should": [
                                            {
                                                "match": {
                                                    "title_analyzed": {
                                                        "query": transformed_query,
                                                        "boost": 10 
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                },
                                "score_mode": "sum",
                                "boost_mode": "replace"
                            }
                        },
                        {
                            "match": {
                                "content_analyzed": transformed_query,
                            }
                        }
                    ]
                }
            }
        })

        # Extraer los documentos de 'hits'
        hits = res['hits']['hits']

        # Crear una lista de resultados únicos
        unique_results = set()
        filtered_res = []

        for hit in hits:
            url = hit["_source"].get("url")
            if url not in unique_results:
                unique_results.add(url)
                filtered_res.append(hit)
        return render_template("results.html", res=filtered_res)
    except:
        print("Error")
        return render_template("404.html")


    
def get_links_from_page(root_url):
    try:
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
        driver.get(root_url)

        for _ in range(3):
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        links = driver.find_elements(By.TAG_NAME, 'a')
        url_extractor = URLExtract()
        urls = set()  # Utilizar un conjunto para almacenar URLs únicas

        for link in links:
            href = link.get_attribute('href')
            if href:
                extracted_urls = url_extractor.find_urls(href)
                urls.update(extracted_urls)

        # Guardar las URLs únicas en un archivo TXT
        with open("urls.txt", "w") as file:
            for url in urls:
                file.write(url + "\n")

        return list(urls)  # Convertir el conjunto a lista antes de devolverlo

    except Exception as e:
        print("Error al obtener enlaces de la página:", str(e))
        return []

    finally:
        if driver:
            driver.quit()


async def process_and_analyze(content, index_name, analyzer_name):
    # Normalizar el contenido eliminando acentos
    normalized_content = unidecode(content)

    # Usar el analizador personalizado de Elasticsearch
    analysis = es.indices.analyze(index=index_name, body={
        "analyzer": analyzer_name,
        "text": normalized_content
    })

    stemmed_tokens = [token_info["token"] for token_info in analysis["tokens"]]
    analyzed_content = " ".join(stemmed_tokens)
    return analyzed_content

async def scrape_and_index(url_list, index_name, analyzer_name, request):
    driver = None
    global cancel_indexing_flag 
    global progress_percentage
    
    try:
        # Verificar si el índice existe
        if not es.indices.exists(index=index_name):
            # Crear el índice si no existe
            index_settings = {
                "settings": {
                    "analysis": {
                        "analyzer": {
                            analyzer_name: {
                                "type": "custom",
                                "tokenizer": "standard",
                                "filter": ["lowercase", "stop", "snowball"]
                            }
                        }
                    }
                }
            }
            es.indices.create(index=index_name, body=index_settings, ignore=400)

        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)

        def close_modal():
            # Función para cerrar el modal si existe
            try:
                modal_element = driver.find_element(By.XPATH, '//div[contains(@class, "modal")]')
                if modal_element.is_displayed():
                    # Cerrar el modal
                    driver.execute_script("arguments[0].style.display='none';", modal_element)
            except:
                pass

        total_pages = len(url_list)
        processed_pages = 0
        documents = []

        for url in url_list:
            # Verificar la bandera de cancelación antes de procesar cada URL
            if cancel_indexing_flag:
                print("Indexing canceled by user.")
                cancel_indexing_flag = False
                progress_percentage = 0.0
                driver.quit()

            driver.get(url)
            time.sleep(2)
            
            # Cerrar el modal si existe
            close_modal()

            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(1)
            title = driver.title
            processed_pages += 1
            percentage = (processed_pages / total_pages) * 100
            message = f"{processed_pages}. Procesando URL: {url} Título: {title} - {percentage:.2f}% del total del sitio ha sido crawleado"
            print(message)
            update_progress(percentage, request)

            # Normalizar el título eliminando acentos
            normalized_title = unidecode(title)

            content = driver.find_elements(By.CSS_SELECTOR, 'p')
            content_ul = driver.find_elements(By.CSS_SELECTOR, 'ul')
            content_title = driver.title
            content_text = ""

            for cont in content:
                text = cont.text.strip()
                if text:
                    content_text += text + "\n"
            for cont in content_ul:
                text = cont.text.strip()
                if text:
                    content_text += text + "\n"
        

            # Aplicar el analizador al contenido y al título
            analyzed_content = await process_and_analyze(content_text, index_name, analyzer_name)
            analyzed_title = await process_and_analyze(normalized_title, index_name, analyzer_name)  # Título procesado con el analizador

            doc = {
                '_op_type': 'index',
                '_index': index_name,
                '_id': str(url),  # Definir '_id' como la URL
                'url': str(url),
                'type': "url",
                'title': str(content_title),  # Mantener el título sin procesar
                'content': str(content_text),
                'timestamp': datetime.now(),
                'content_analyzed': analyzed_content,
                'title_analyzed': analyzed_title  # Título procesado con el analizador
            }
            documents.append(doc)
        for doc in documents:
            # Verificar si el documento ya existe en el índice
            if es.exists(index=index_name, id=doc['_id']):
                existing_doc = es.get(index=index_name, id=doc['_id'])
                # Actualizar el contenido del documento existente
                existing_doc['_source']['content'] = doc['content']
                existing_doc['_source']['title'] = doc['title']
                # Utilizar 'doc' para la actualización
                es.update(index=index_name, id=doc['_id'], body={"doc": existing_doc['_source']})
            else:
                # Si no existe, agregar el nuevo documento
                es.index(index=index_name, id=doc['_id'], body=doc, ignore=400)

        success, failed = bulk(es, documents, index=index_name, raise_on_error=False)
        print(f"Documentos agregados con éxito: {success}")
        print(f"Documentos fallidos: {failed}")
        
    except Exception as e:
        pass
    finally:
        if driver:
            driver.quit()

    return []

progress_percentage = 0.0

@app.route('/cancel_indexing', methods=['GET'])
def cancel_indexing():
    global cancel_indexing_flag
    cancel_indexing_flag = True
    return jsonify(success=True)

def update_progress(percentage, request):
    # Enviar el progreso actual a través de AJAX
    global progress_percentage
    progress_percentage = percentage  # Actualizar la variable global
    request_data = {"percentage": percentage}
    return jsonify(request_data)


@app.route('/get_progress', methods=['GET'])
def get_progress():
    global progress_percentage
    return jsonify({"percentage": progress_percentage})


def transform_query(query, index_name, analyzer_name):
    """
    Transforma el texto de la consulta para que coincida con la estructura del documento.
    Realiza la eliminación de acentos y utiliza el analizador personalizado de Elasticsearch.
    """
    try:
        # Verificar si el índice existe
        if not es.indices.exists(index=index_name):
            # Crear el índice si no existe
            index_settings = {
                "settings": {
                    "analysis": {
                        "analyzer": {
                            analyzer_name: {
                                "type": "custom",
                                "tokenizer": "standard",
                                "filter": ["lowercase", "stop", "snowball"]
                            }
                        }
                    }
                }
            }
            es.indices.create(index=index_name, body=index_settings, ignore=400)

        # Elimina acentos y convierte a minúsculas
        normalized_query = unidecode(query.lower())

        # Normaliza el contenido eliminando acentos
        normalized_query_content = unidecode(normalized_query)

        # Usa el analizador personalizado de Elasticsearch
        analysis = es.indices.analyze(index=index_name, body={
            "analyzer": analyzer_name,
            "text": normalized_query_content
        })

        stemmed_tokens_query = [token_info["token"] for token_info in analysis["tokens"]]
        analyzed_query_content = " ".join(stemmed_tokens_query)

        return analyzed_query_content

    except:
        print(f"Error:")
        # Manejar el caso cuando el índice no existe
        return None

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5003)
