#!/bin/bash

# Nombre deseado para el entorno virtual
venv_name="virtual_env"

# Verificar si python3-venv está instalado
if ! command -v python3 -m venv &> /dev/null
then
    echo "python3-venv no está instalado. Instalando..."
    # Asegurarse de tener instalado el paquete necesario
    sudo apt-get update
    sudo apt-get install -y python3-venv
fi

# Crear el entorno virtual con el nombre deseado
python3 -m venv "$venv_name"

# Activar el entorno virtual
source "$venv_name/bin/activate"

# Instalar dependencias desde requirements.txt
pip install -r requirements.txt

# Asegurar que el script tiene permisos de ejecución
chmod +x install.sh
