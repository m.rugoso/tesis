function showErrorPopup(message) {
    var errorPopup = document.getElementById('error-popup');
    var errorMessage = document.getElementById('error-message');
    
    errorMessage.textContent = message;
    errorPopup.style.display = 'block';
    
    var closePopup = document.getElementById('close-popup');
    closePopup.addEventListener('click', function() {
        errorPopup.style.display = 'none';
    });
}